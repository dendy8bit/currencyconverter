//
//  CurrencyViewModel.swift
//  CurrencyConverter
//
//  Created by Alexander Karenski on 8.02.22.
//

import Foundation
import UIKit

class CurrencyViewModel {
    
    //MARK: - Properties
    private var currencyValue           : CurrencyValue?
    private var firstKeyAndFlagValue    : String?
    private var secondKeyAndFlagValue   : String?
    private var firstCurrencyID         : CurrencyID?
    private var secondCurrencyID        : CurrencyID?
    
    //MARK: - Functions
    func updateCurrency(currancyID: CurrencyID, complitionHandler: @escaping (Error?) -> Void) {
        
        NetworkManager.shared.fetchCurrency(currencyID: currancyID) { [weak self] result in
            
            guard
                let self = self
            else {
                return
            }
            
            switch result {
            case .success(let currencyValue):
                
                self.currencyValue = currencyValue
                complitionHandler(nil)
                break
            case .failure(let error):
                
                complitionHandler(error)
            }
        }
    }
    
    func getCurrencyValue(by id: CurrencyID) -> Double {
        
        guard
            let currency = currencyValue
        else {
            return 0.0
        }
        
        switch id {
        case .USD:
            return currency.usdValue
        case .EUR:
            return currency.eurValue
        case .RUB:
            return currency.rubValue
        case .BYR:
            return currency.bynValue
        }
    }
    
    func getCurrencyValue() -> CurrencyValue {
        
        return currencyValue ?? CurrencyValue(eurValue: 0.0,
                                              rubValue: 0.0,
                                              bynValue: 0.0,
                                              usdValue: 0.0)
    }
    
    func getAllCodeAndFlagCurrency() -> [String] {
        
        return [CurrencyID.BYR.rawValue + (CurrencyID.flagsByCurrencies[.BYR] ?? ""),
                CurrencyID.USD.rawValue + (CurrencyID.flagsByCurrencies[.USD] ?? ""),
                CurrencyID.EUR.rawValue + (CurrencyID.flagsByCurrencies[.EUR] ?? ""),
                CurrencyID.RUB.rawValue + (CurrencyID.flagsByCurrencies[.RUB] ?? "")]
    }
    
    func getCodeAndFlagCurrency(by index: Int) -> String {
        
        return getAllCodeAndFlagCurrency()[index]
    }
    
    func setValuesForCurrency(firstValue: String, secondValue: String) {
        
        self.firstKeyAndFlagValue  = firstValue
        self.secondKeyAndFlagValue = secondValue
    }
    
    func getValuesForCurrency() -> [String?] {
        
        return [firstKeyAndFlagValue, secondKeyAndFlagValue]
    }
    
    func setCurrencyID(firstCurrencyID: Int, secondCurrencyID: Int) {
        
        self.firstCurrencyID  = getID(by: firstCurrencyID)
        self.secondCurrencyID = getID(by: secondCurrencyID)
    }
    
    func setCurrencyID(firstCurrencyID: CurrencyID, secondCurrencyID: CurrencyID) {
        
        self.firstCurrencyID  = firstCurrencyID
        self.secondCurrencyID = secondCurrencyID
    }
    
    func getFirstCurrencyID() -> CurrencyID {
        
        guard let id = firstCurrencyID else { return CurrencyID.USD }
        return id
    }
    
    func getSecondCurrencyID() -> CurrencyID {
        
        guard let id = secondCurrencyID else { return CurrencyID.USD }
        return id
    }
    
    func calculationCurrency(sender: UITextField, method: (Double, Double) -> Double) -> String {
        
        guard
            let text = sender.text,
            let value = Double(text)
        else {
            return ""
        }
        
        return calculation(method: method, by: value)
    }
    
    private func getID(by id: Int) -> CurrencyID {
        
        switch id {
        case 0:
            return .BYR
        case 1:
            return .USD
        case 2:
            return .EUR
        case 3:
            return .RUB
        default:
            return .BYR
        }
    }
    
    //Exchange rate calculation
    private func calculation(method: (Double, Double) -> Double, by value: Double) -> String {
        
        guard
            let currenceID = secondCurrencyID
        else {
            return ""
        }
        
        let currentCurrency = getCurrencyValue(by: currenceID)
        
        return String(format: "%.4f", method(value, currentCurrency))
    }
}
