//
//  NetworkManager.swift
//  CurrencyConverter
//
//  Created by Alexander Karenski on 8.02.22.
//

import Foundation

enum CurrencyID : String, CaseIterable {
    
    case BYR = "BYR"
    case USD = "USD"
    case EUR = "EUR"
    case RUB = "RUB"

    static let flagsByCurrencies : [CurrencyID : String] = [
        .BYR : "🇧🇾",
        .USD : "🇺🇸",
        .EUR : "🇪🇺",
        .RUB : "🇷🇺",
    ]
}

final class NetworkManager {
    
    static let shared = NetworkManager()
    
    func fetchCurrency(currencyID: CurrencyID, complitionHandler: @escaping (Result<CurrencyValue, Error>) -> Void) {
        
        guard
            let url = URL(string: "https://freecurrencyapi.net/api/v2/latest?apikey=1b206560-88fe-11ec-a398-cbac765900f4&base_currency=\(currencyID)")
        else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let error = error {
                complitionHandler(.failure(error))
            }
            
            guard
                let data = data
            else {
                return
            }

            do {
                let decodeData = try JSONDecoder().decode(APICurrency.self, from: data)
                let currancy = CurrencyValue(apiResponse: decodeData)
                complitionHandler(.success(currancy))
            } catch let error{
                complitionHandler(.failure(error))
            }
        }.resume()
    }
}
