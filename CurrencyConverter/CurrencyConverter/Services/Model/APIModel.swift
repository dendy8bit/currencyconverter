//
//  APIModel.swift
//  CurrencyConverter
//
//  Created by Alexander Karenski on 8.02.22.
//

import Foundation

// MARK: - Welcome
struct APICurrency: Codable {
    let query: Query?
    let data : [String: Double]?
}

// MARK: - Query
struct Query: Codable {
    let apikey      : String?
    let timestamp   : Int?
    let baseCurrency: String?

    enum CodingKeys: String, CodingKey {
        case apikey, timestamp
        case baseCurrency = "base_currency"
    }
}
