//
//  CurrencyValue.swift
//  CurrencyConverter
//
//  Created by Alexander Karenski on 8.02.22.
//

import Foundation

class CurrencyValue {
    
    let eurValue: Double
    let rubValue: Double
    let bynValue: Double
    let usdValue: Double
    
    init(apiResponse: APICurrency) {
        
        self.eurValue = apiResponse.data?[CurrencyID.EUR.rawValue] ?? 0.0
        self.rubValue = apiResponse.data?[CurrencyID.RUB.rawValue] ?? 0.0
        self.bynValue = apiResponse.data?[CurrencyID.BYR.rawValue] ?? 0.0
        self.usdValue = apiResponse.data?[CurrencyID.USD.rawValue] ?? 0.0
    }
    
    init(eurValue: Double, rubValue: Double, bynValue: Double, usdValue: Double) {
        
        self.eurValue = eurValue
        self.rubValue = rubValue
        self.bynValue = bynValue
        self.usdValue = usdValue
    }
}
