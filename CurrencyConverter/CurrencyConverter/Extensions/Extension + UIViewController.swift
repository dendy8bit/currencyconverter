//
//  Extension + UIViewController.swift
//  CurrencyConverter
//
//  Created by Alexander Karenski on 9.02.22.
//

import Foundation
import UIKit

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    func showAlertControl(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .default) { action in
            
        }
        alertController.addAction(actionOK)
        self.present(alertController, animated: true, completion: nil)
    }
}
