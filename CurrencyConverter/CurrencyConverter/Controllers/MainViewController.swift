//
//  ViewController.swift
//  CurrencyConverter
//
//  Created by Alexander Karenski on 8.02.22.
//

import UIKit

class MainViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var getExchangeButton: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    
    //MARK: - Properties
    private let viewModel = CurrencyViewModel()
    
    //MARK: - LifeCycle func
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getExchangeButton.layer.cornerRadius = 15
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard
            segue.identifier == "CurrencyId",
            let currencyViewController = segue.destination as? CurrencyViewController
        else {
            return
        }
        
        let firstCodeAndFlag = self.viewModel.getCodeAndFlagCurrency(by: pickerView.selectedRow(inComponent: 0))
        let secondCodeAndFlag = self.viewModel.getCodeAndFlagCurrency(by: pickerView.selectedRow(inComponent: 1))
        
        self.viewModel.setValuesForCurrency(firstValue: firstCodeAndFlag, secondValue: secondCodeAndFlag)
        self.viewModel.setCurrencyID(firstCurrencyID: pickerView.selectedRow(inComponent: 0),
                                secondCurrencyID: pickerView.selectedRow(inComponent: 1))
        
        currencyViewController.setViewModel(viewModel: self.viewModel)
    }
}

//MARK: - UIPickerViewDelegate
extension MainViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return self.viewModel.getAllCodeAndFlagCurrency()[row]
    }
}

//MARK: - UIPickerViewDataSource
extension MainViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        self.viewModel.getAllCodeAndFlagCurrency().count
    }
}
