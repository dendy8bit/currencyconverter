//
//  CurrencyViewController.swift
//  CurrencyConverter
//
//  Created by Alexander Karenski on 8.02.22.
//

import UIKit

class CurrencyViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var firstCurrencyLabel: UILabel!
    @IBOutlet weak var secondCurrencyLabel: UILabel!
    @IBOutlet weak var firstCurrencyValueTextField: UITextField!
    @IBOutlet weak var secondCurrencyValueTextField: UITextField!
    
    //MARK: - Properties
    lazy private var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        activityIndicator.style = .medium
        return activityIndicator
    }()
    
    lazy private var barButton: UIBarButtonItem = {
        let barButton = UIBarButtonItem()
        barButton.customView = activityIndicator
        return barButton
    }()
    
    private var viewModel = CurrencyViewModel()
    
    //MARK: - LifeCycle func
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.viewModel.updateCurrency(currancyID: self.viewModel.getFirstCurrencyID()) { error in
            
            if let error = error {
                DispatchQueue.main.async { [weak self] in
                    
                    guard
                        let self = self
                    else {
                        return
                    }
                    
                    self.activityIndicator.stopAnimating()
                    self.navigationItem.title = "Exchange"
                    
                    self.showAlertControl(title: "Error", message: error.localizedDescription)
                }
            }
            
            DispatchQueue.main.async { [weak self] in
                
                guard
                    let self = self
                else {
                    return
                }
                
                let id = self.viewModel.getSecondCurrencyID()
                let currencyValueByID = self.viewModel.getCurrencyValue(by: id)
                
                self.firstCurrencyValueTextField.text = String(1)
                self.secondCurrencyValueTextField.text = String(currencyValueByID)
                self.activityIndicator.stopAnimating()
                self.navigationItem.title = "Exchange"
            }
        }
        
        self.view.backgroundColor = .systemBackground
        self.navigationItem.setRightBarButton(barButton, animated: true)
        
        self.activityIndicator.startAnimating()
        self.navigationItem.title = "Loading Data"
        
        //hide keyboard when end editing
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.firstCurrencyLabel.text = self.viewModel.getValuesForCurrency().first as? String
        self.secondCurrencyLabel.text = self.viewModel.getValuesForCurrency().last as? String
    }
    
    //MARK: - Actions
    @IBAction func firstCurrencyValueChanged(_ sender: UITextField) {

        self.secondCurrencyValueTextField.text = self.viewModel.calculationCurrency(sender: sender, method: { firstCurrency, secondCurrency in
            firstCurrency * secondCurrency
        })
    }
    
    @IBAction func secondCurrencyValueChanged(_ sender: UITextField) {

        self.firstCurrencyValueTextField.text = self.viewModel.calculationCurrency(sender: sender, method: { firstCurrency, secondCurrency in
            firstCurrency / secondCurrency
        })
    }
    
    
    @IBAction func changeValuesCurrencyDidTap(_ sender: UIButton) {
        
        let firstValueCurrency = self.firstCurrencyLabel.text
        let secondValueCurrency = self.secondCurrencyLabel.text
        let tempFirstCurrencyID = self.viewModel.getFirstCurrencyID()
        let tempSecondCurrencyID = self.viewModel.getSecondCurrencyID()
        
        self.firstCurrencyLabel.text = secondValueCurrency
        self.secondCurrencyLabel.text = firstValueCurrency
        self.viewModel.setCurrencyID(firstCurrencyID: tempSecondCurrencyID, secondCurrencyID: tempFirstCurrencyID)
        
        self.firstCurrencyValueTextField.text = ""
        self.secondCurrencyValueTextField.text = ""
        
        self.viewModel.updateCurrency(currancyID: tempSecondCurrencyID) { error in
            
            if let error = error {
                
                DispatchQueue.main.async { [weak self] in
                    
                    guard let self = self else { return }
                    
                    self.activityIndicator.stopAnimating()
                    self.navigationItem.title = "Exchange"
                    
                    self.showAlertControl(title: "Error", message: error.localizedDescription)
                }
            }
            
            DispatchQueue.main.async { [weak self] in
                
                guard let self = self else { return }
                
                let id = self.viewModel.getSecondCurrencyID()
                let currencyValueByID = self.viewModel.getCurrencyValue(by: id)
                
                self.firstCurrencyValueTextField.text = String(1)
                self.secondCurrencyValueTextField.text = String(currencyValueByID)
                self.activityIndicator.stopAnimating()
                self.navigationItem.title = "Exchange"
            }
        }
        
        activityIndicator.startAnimating()
        self.navigationItem.title = "Loading Data"
    }
    
    //MARK: - Func
    func setViewModel(viewModel: CurrencyViewModel) {
        
        self.viewModel = viewModel
    }
}
